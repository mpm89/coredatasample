//
//  Utiliy.swift
//  CoredataSample
//
//  Created by User on 03/03/21.
//

import Foundation
import UIKit

extension UIView {

  // Export pdf from Save pdf in drectory and return pdf file path
    func exportAsPdfFromView(stringFileName:String) -> String {

      let pdfPageFrame = self.bounds
      let pdfData = NSMutableData()
      UIGraphicsBeginPDFContextToData(pdfData, pdfPageFrame, nil)
      UIGraphicsBeginPDFPageWithInfo(pdfPageFrame, nil)
      guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
      self.layer.render(in: pdfContext)
      UIGraphicsEndPDFContext()
        return self.saveViewPdf(data: pdfData, getName: stringFileName)

  }

  // Save pdf file in document directory
    func saveViewPdf(data: NSMutableData,getName:String) -> String {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    let docDirectoryPath = paths[0]
    let pdfPath = docDirectoryPath.appendingPathComponent(getName)
    if data.write(to: pdfPath, atomically: true) {
        return pdfPath.path
    } else {
        return ""
    }
  }
}

// check empty string
extension StringProtocol where Index == String.Index {
    var isEmptyField: Bool {
        return trimmingCharacters(in: .whitespaces) == ""
    }
}
