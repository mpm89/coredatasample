//
//  User+CoreDataProperties.swift
//  CoredataSample
//
//  Created by User on 03/03/21.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var firstname: String?
    @NSManaged public var lastname: String?
    @NSManaged public var dateofbirth: Date?
    @NSManaged public var phone: String?
    @NSManaged public var pdfFile: String?
    @NSManaged public var relationship: User?

}

extension User : Identifiable {

}
