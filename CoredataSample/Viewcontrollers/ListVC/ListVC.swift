//
//  ListVC.swift
//  CoredataSample
//
//  Created by User on 03/03/21.
//

import UIKit
import CoreData
class ListVC: UIViewController {
    var allUsers : [User] = []
    
    @IBOutlet weak var tableUser: UITableView!
    @IBOutlet weak var labelNodata: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.allUsers = fetchAllRecords()
        if self.allUsers.count == 0{
            self.labelNodata.isHidden = false
        }
        self.tableUser.reloadData()
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func fetchAllRecords() -> [User] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            return try context.fetch(request) as? [User] ?? []
        } catch {
            return []
        }
    }
    
    func convertDateTosstring(getDate:Date) -> String? {
        let dayFormatter = DateFormatter()
        dayFormatter.dateFormat =  "dd-MM-yyyy"
        let DOBDate = dayFormatter.string(from: getDate)
        return DOBDate
    }
    func fetchPdf(pdfString: String) -> URL? {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        if let path = paths.first {
            return URL(fileURLWithPath: path).appendingPathComponent(pdfString)
           
           
        }
        return nil
    }
}
extension ListVC :UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allUsers.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as? ListCell
        
        cell?.populateUserData(name: "\(allUsers[indexPath.row].firstname ?? "") \(allUsers[indexPath.row].lastname ?? "")", Dob: self.convertDateTosstring(getDate: allUsers[indexPath.row].dateofbirth ?? Date()), phone: allUsers[indexPath.row].phone ?? "")
        
        return cell ?? UITableViewCell()
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pfdVC = self.storyboard?.instantiateViewController(identifier: "PdfViewVC") as! PdfViewVC
        pfdVC.fileUrlPath = self.fetchPdf(pdfString: allUsers[indexPath.row].pdfFile ?? "")
        self.navigationController?.pushViewController(pfdVC, animated: true)
    }
    
    
    
    
}
