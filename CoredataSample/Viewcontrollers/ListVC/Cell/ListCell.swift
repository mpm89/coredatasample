//
//  ListCell.swift
//  CoredataSample
//
//  Created by User on 03/03/21.
//

import UIKit

class ListCell: UITableViewCell {

    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblDob: UILabel!
    @IBOutlet weak var labelName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func populateUserData(name:String?,Dob:String?,phone:String?) {
        lblPhone.text = "Phone Number: \(phone ?? "")"
        lblDob.text = "D.O.B: \(Dob ?? "")"
        labelName.text = "Name: \(name ?? "")"
    }

}
