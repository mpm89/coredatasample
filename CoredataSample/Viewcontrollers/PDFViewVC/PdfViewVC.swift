//
//  PdfViewVC.swift
//  CoredataSample
//
//  Created by User on 03/03/21.
//

import UIKit
import PDFKit

class PdfViewVC: UIViewController {
    @IBOutlet weak var viewframeShow: UIView!
    @IBOutlet weak var viewHeader: UIView!
    var fileUrlPath :URL?
    var pdfFrame:CGRect?
    override func viewDidLoad() {
        super.viewDidLoad()
        let pdfView = PDFView(frame: CGRect(x: 0, y: viewHeader.frame.height, width: self.view.frame.width, height: self.view.frame.height - (viewHeader.frame.height)))
        guard let url = fileUrlPath else {
            return
        }
        if let pdfDocument = PDFDocument(url: url) {
            pdfView.displayMode = .singlePage
            pdfView.autoScales = true
            pdfView.displayDirection = .vertical
            pdfView.document = pdfDocument
            view.addSubview(pdfView)
            
        }
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        viewframeShow.frame = self.view.frame
    }
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
