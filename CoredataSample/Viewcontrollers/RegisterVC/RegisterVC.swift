//
//  ViewController.swift
//  CoredataSample
//
//  Created by User on 02/03/21.
//

import UIKit
import CoreData

class RegisterVC: UIViewController {
    var isselectDOB :Bool = false
    @IBOutlet weak var textFieldDOB: UITextField!
    @IBOutlet weak var textFieldPhone: UITextField!
    @IBOutlet weak var textFieldFirstName: UITextField!
    @IBOutlet weak var textFieldLastName: UITextField!
    @IBOutlet weak var signatureView: Signature!
    @IBOutlet weak var viewFrametoGenerate: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        signatureView.setStrokeColor(color: .black)
    }
    
    //MARK: Custom Methods
    func setupViews(){
        
        signatureView.layer.borderWidth = 0.5
        signatureView.layer.borderColor = UIColor.black.cgColor
        signatureView.layer.cornerRadius = 10
        createDatePickerView(textField: textFieldDOB)
        createToolBarKeyboard(textField: textFieldPhone)
        createToolBarKeyboard(textField: textFieldFirstName)
        createToolBarKeyboard(textField: textFieldLastName)
    }
    func clearAllFields() {
        textFieldPhone.text = ""
        textFieldDOB.text = ""
        textFieldLastName.text = ""
        textFieldFirstName.text = ""
        signatureView.clear()
    }
    
    //save in user details in core data
    func saveUser(pdfFileName:String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "User", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context) as? User
        newUser?.firstname = textFieldFirstName.text
        newUser?.lastname = textFieldLastName.text
        newUser?.dateofbirth = Date()
        newUser?.phone = textFieldPhone.text
        newUser?.pdfFile = pdfFileName
        do {
            try context.save()
            self.showAlertAction(title: "Success", message: "Your record is successfully saved")
            self.clearAllFields()
        } catch {
            print("Failed saving")
        }
    }
    
    func showAlertAction(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            print("Action")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //date conversion
    func convertDateTosstring(getDate:Date) -> String? {
        let dayFormatter = DateFormatter()
        dayFormatter.dateFormat =  "dd-MM-yyyy"
        let DOBDate = dayFormatter.string(from: getDate)
        return DOBDate
    }
    func convertsstringTodate(dateString:String) -> Date? {
        let dayFormatter = DateFormatter()
        dayFormatter.dateFormat =  "dd-MM-yyyy"
        let DOBDate = dayFormatter.date(from: dateString)
        return DOBDate
    }
    //MARK: Date Picker
    func createToolBarKeyboard(textField:UITextField) {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.dissmissKeyboard(sender:)))
        button.tag = textField.tag
        
       
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spacer,button], animated: true)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    func createDatePickerView(textField:UITextField) {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        datePicker.maximumDate = Date()
        textField.inputView = datePicker
        datePicker.tag = textField.tag
        datePicker.addTarget(self, action: #selector(self.handleDatePicker(sender:)), for: UIControl.Event.valueChanged)
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.done(sender:)))
        button.tag = textField.tag
        
        let cancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelDatePicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([cancel,spacer,button], animated: true)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
        
    }
    @objc func handleDatePicker(sender: UIDatePicker) {
        
        self.textFieldDOB.text = convertDateTosstring(getDate: sender.date)
        isselectDOB = true
        
        
    }
    @objc func cancelDatePicker(){
        isselectDOB = false
    }
    @objc func done(sender:UIBarButtonItem) {
        if isselectDOB == false{
            self.textFieldDOB.text = convertDateTosstring(getDate: Date())
        }
        self.view.endEditing(true)
    }
    @objc func dissmissKeyboard(sender:UIBarButtonItem) {
       
        self.view.endEditing(true)
    }
    
    //MARK: IBACTIONS
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        guard (!(textFieldFirstName.text?.isEmptyField ?? false)) else {
            self.showAlertAction(title: "", message: "Please enter first name")
            return
        }
        guard (!(textFieldLastName.text?.isEmptyField ?? false)) else {
            self.showAlertAction(title: "", message: "Please enter last name")
            return
        }
        guard (!(textFieldDOB.text?.isEmptyField ?? false) ) else {
            self.showAlertAction(title: "", message: "Please select date of birth")
            return
        }
        guard (!(textFieldPhone.text?.isEmptyField ?? false)) else {
            self.showAlertAction(title: "", message: "Please enter phone number")
            return
        }
        
       
            let timeStamp = Date().timeIntervalSince1970
            let pdfFilePath = viewFrametoGenerate.exportAsPdfFromView(stringFileName: "\(timeStamp).pdf")
            print(pdfFilePath)
       
        
        self.saveUser(pdfFileName: "\(timeStamp).pdf")
    }
    @IBAction func clearBtnTapped(_ sender: UIButton) {
        
        signatureView.clear()
    }
    
    @IBAction func btnListShowAction(_ sender: UIButton) {
        let listVC = self.storyboard?.instantiateViewController(identifier: "ListVC") as! ListVC
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    
}

